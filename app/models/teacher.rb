class Teacher < ActiveRecord::Base
  has_many :students
  validates_uniqueness_of :email
end
